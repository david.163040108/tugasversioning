package id.ac.unpas.kakas.math;

import id.ac.unpas.kakas.impl.AddAndSubInterface;

public class A implements AddAndSubInterface {

	public A() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "A []";
	}

	// tambah
	public int add(int n1, int n2) {
		int hasil = n1 + n2;
		return hasil;
	}

	// kurang
	public int sub(int n1, int n2) {
		int hasil = n1 - n2;
		return hasil;
	}
}
