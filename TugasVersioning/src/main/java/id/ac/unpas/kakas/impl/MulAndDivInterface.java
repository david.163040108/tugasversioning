package id.ac.unpas.kakas.impl;

public interface MulAndDivInterface {
	public double mul (double n1 , double n2);
	public double div (double n1, double n2);

}
