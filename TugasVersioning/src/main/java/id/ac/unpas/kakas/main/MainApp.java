package id.ac.unpas.kakas.main;

import static spark.Spark.get;

import java.util.HashMap;
import org.apache.velocity.app.VelocityEngine;
import id.ac.unpas.kakas.math.C;
import id.ac.unpas.kakas.math.D;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

public class MainApp {
	public static void main(String[] args) {
		VelocityEngine configuredEngine = new VelocityEngine();
		configuredEngine.setProperty("runtime.references.strict", true);
		configuredEngine.setProperty("resource.loader", "class");
		configuredEngine.setProperty("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		VelocityTemplateEngine velocityTemplateEngine = new VelocityTemplateEngine(configuredEngine);

		get("/kalkulator1", (req, res) -> {
			HashMap<String, Object> model = new HashMap<>();
			String bString1 = req.queryParamOrDefault("bil1", "0");
			String bString2 = req.queryParamOrDefault("bil2", "0");
			String math = req.queryParamOrDefault("math", "");
			double bInput1 = Double.parseDouble(bString1);
			double bInput2 = Double.parseDouble(bString2);
			int n1 = (int) Math.round(bInput1);
			int n2 = (int) Math.round(bInput2);

			C c = new C(bInput1, bInput2);

			if (math.equals("add")) {
				model.put("hasil", c.add());
			} else if (math.equals("sub")) {
				model.put("hasil", c.sub());
			} else if (math.equals("mul")) {
				model.put("hasil", c.mul());
			} else if (math.equals("div")) {
				model.put("hasil", c.div());
			} else if (math.equals("mul")) {
				model.put("hasil", c.mul());
			} else if (math.equals("mod")) {
				model.put("hasil", c.mod(n1, n2));
			} else if (math.equals("sqrt")) {
				model.put("hasil", c.sqrt(bInput1));
			} else {
				model.put("hasil", "0");
			}

			model.put("kelas", c.toString());
			model.put("bilInput1", bInput1);
			model.put("bilInput2", bInput2);
			String templatePath = "/view/index.html";
			return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
		});

		get("/kalkulator2", (req, res) -> {
			HashMap<String, Object> model = new HashMap<>();
			String bString1 = req.queryParamOrDefault("bil1", "0");
			String bString2 = req.queryParamOrDefault("bil2", "0");
			String math = req.queryParamOrDefault("math", "");
			double bInput1 = Double.parseDouble(bString1);
			double bInput2 = Double.parseDouble(bString2);
			D d = new D();

			if (math.equals("pow")) {
				model.put("hasil", d.pow(bInput1, bInput2));
			} else if (math.equals("mul")) {
				model.put("hasil", d.mul(bInput1, bInput2));
			} else if (math.equals("div")) {
				model.put("hasil", d.div(bInput1, bInput2));
			} else {
				model.put("hasil", "0");
			}

			model.put("kelas", d.toString());
			model.put("bilInput1", bInput1);
			model.put("bilInput2", bInput2);
			String templatePath = "/view/kalkulator2.html";
			return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
		});
	}
}
